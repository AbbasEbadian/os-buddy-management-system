#include<iostream>
using namespace std;
class Node
{
public:
	int size;
	bool Empty;
	int label;
	Node *left, *right,*parent;
	Node()
	{
		left = right = parent = NULL;
		size = 2048;
		label = -1;
		Empty = 1;
	}
	Node(int x)
	{
		left = right = parent = NULL;
		size = x;
		label = -1;
		Empty = 1;
	}
	Node(int x,Node*p)
	{
		left = right = NULL;
		parent = p;
		size = x;
		label = -1;
		Empty = 1;
	}
};
class BodySystem
{
	Node *root;
public:
	BodySystem()
	{
		root = new Node();
	}
	BodySystem(int x)
	{
		root = new Node(x);
	}
	bool allocation( Node *a, int x, int p)
	{
		if (a->left==NULL)
		{
			if (a->Empty)
			{
				if (a->size < x)
					return false;
				if (a->size >= x && (a->size / 2) < x)
				{
					a->label = p;
					a->Empty = false;
					return true;
				}
				else
				{
					int k = a->size / 2;
					Node *temp1 = new Node(k,a), *temp2 = new Node(k,a);
					a->left = temp1;
					a->right = temp2;
					a->Empty = false;
					return allocation(a->left, x, p);
				}
			}
			return false;
		}
		else
		{
			if (allocation(a->left, x, p))
				return true;
			return allocation(a->right, x, p);
		}
		return false;
	}
	void allocating(int x, int p)
	{
		if (allocation(root, x, p))
			cout << "\ntakhsis ba movafaghiat anjam shod" << endl;
		else
			cout << "\ntakhsis ba moshkel movajeh shod" << endl;
	}
	void Integration(Node *a)
	{
		Node*temp = a->parent;
		if(temp)
			while (temp->left->Empty&&temp->right->Empty)
			{
				delete(temp->left);
				delete(temp->right);
				temp->left = temp->right = NULL;
				temp->Empty = 1;
				if (temp->parent)
					temp = temp->parent;
				else
					return;
			}
	}
	bool release(Node *a, int p)
	{
		if (a->left == NULL)
		{
			if (!a->Empty)
			{
				if (a->label == p)
				{
					a->label = -1;
					a->Empty = 1;
					Integration(a);
					return true;
				}
			}
		}
		else
		{
			if (release(a->left, p))
				return true;
			return release(a->right, p);
		}
		return false;
	}
	void Release(int p)
	{
		if (release(root, p))
			cout << "\nazad sazi ba movafaghiat anjam shod" << endl;
		else
			cout << "\nazad sazi ba moshkel movajeh shod" << endl;
	}
	void printpreorder(Node *a)
	{
		cout << a->size << "," << a->label << "-";
		if (a->left)
			printpreorder(a->left);
		if (a->right)
			printpreorder(a->right);
	}
	void printinorder(Node *a)
	{
		if (a->left)
			printinorder(a->left);
		cout << a->size << "," << a->label << "-";
		if (a->right)
			printinorder(a->right);
	}
	void print()
	{
		cout << "preorder :" << endl;
		printpreorder(root);
		cout << "\ninorder :" << endl;
		printinorder(root);
	}
};

int main()
{
	int x;
	cout << "Total memory: ";
	cin >> x;
	BodySystem B(x);
	cout << "action: ";
	char s;
	cin >> s ;
	while (s != 'e')
	{
		if (s == 'a')
		{
			cout << "process id:";
			int p;
			cin >> p;
			cout << "size:";
			cin >> x ;
			B.allocating(x, p);
		}
		else if (s == 'r')
		{
			int p;
			cin >> p;
			B.Release(p);
		}
		B.print();
		cout << "\n\naction: ";
		cin >> s ;
	}
	
}
